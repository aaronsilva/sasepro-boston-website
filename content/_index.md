## Our Story

The Society of Asian Scientists and Engineers (SASE) is a national professional organization that aims to help Asian heritage scientific and engineering professionals achieve their full potential.

SASE Professional Boston chapter will give working professionals the opportunity to build their network, continue to develop leadership and professional skills, and celebrate diversity in the workplace.
